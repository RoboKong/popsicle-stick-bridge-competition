from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5 import QtWidgets
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import QTimer
# Sorry about the mess.
# Importing resources, ie images and db
import res
import sys, os, time
from gpiozero import LED as led, PWMOutputDevice as out, Button as gp, Motor

# Needs to be imported as "hx" to work later on
# pin 38 is the data pin, pin 40 is the sclk.
from hx711 import HX711
from HX711 import *
from os import path

def resource_path(relative_path):
	""" Get absolute path to resource, works for dev and for PyInstaller """
	base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
	return os.path.join(base_path, relative_path)

from PyQt5.uic import loadUiType

FORM_CLASS,_=loadUiType(resource_path("main.ui"))

import sqlite3

# class Worker(QRunnable):
# 	def __init__(self):
# 		super().__init__()
# 		self.threadpool = QThreadPool()
# 		# self.HX7 = HX711()
# 		self.weight = pyqtSignal()
# 	'''
# 	Worker thread
# 	'''
# 	@pyqtSlot()
# 	def run(self):
# 		self.weight = self.HX7.get_weight(3)
# 		# self.lbl_weight.display(weight)
# 		time.sleep(3)
# 		self.weight.emit()

class Main(QMainWindow, FORM_CLASS):
	def __init__(self,parent=None):
		# Main window, and initiation of GUI
		super(Main,self).__init__(parent)
		QMainWindow.__init__(self)
		self.ui = FORM_CLASS()
		self.setupUi(self)
		# Actually not called yet, can be used to set full screen
		self.init_values()
		self.init_GPIO()
		self.start_btn.clicked.connect(self.START)
		self.stop_btn.clicked.connect(self.STOP)
		self.reverse_btn.clicked.connect(self.REVERSE)
		self.brake_btn.clicked.connect(self.BRAKE)
		self.forward_btn.clicked.connect(self.FORWARD)
		self.back_btn.clicked.connect(self.BACKWARD)
		self.calibrate_btn.clicked.connect(self.CALIBRATE)
		self.tare_btn.clicked.connect(self.TARE)
		self.refresh_btn.clicked.connect(self.GET_DATA)
		self.add_btn.clicked.connect(self.ADD)
		# self.threadpool = QThreadPool()
		self.timer=QTimer()
		self.timer.timeout.connect(self.updateWeight)

	# Doesn't work correctly
	# def setupUi(self):
	# 	self.forward_btn.clicked= self.FORWARD

	def init_values(self):
		self.LoadValue = 0
		self.running = False
		self.bounce = 0.5
		self.PWM = .95
		self.off = 0
		self.frequency = 2000
		#these are the GPIO ports we used for our project
		# Pins as they are wired on the board
		# Orange R_EN #27
		# Yellow L_EN #4
		# White LPWM #17
		# Green RPWM #22
		self.GPIO4 = 4    #This is L_EN of the hbridge
		self.GPIO12 = 12  #This is the Reverse Button Control
		self.GPIO13 = 13  #This is the Stop Button control
		self.GPIO17 = 17  #This is LPWM of the bridge
		self.GPIO20 = 20  #GPIO20 is the Data clock of our Amplifier, the HX711
		self.GPIO21 = 21  #GPIO21 is the Clock for the amplifier timing.
		self.GPIO22 = 22  #This controls RPWM for the hbridge.
		self.GPIO24 = 24  #This controlls the LED of the Start Button. Visual Feedback.
		self.GPIO25 = 25  #This controls the Start Button
		self.GPIO27 = 27  #This is R_EN of the Hbridge LPWM
		self.HX7 = HX711()
		# First Load Cell Calibration Reference
		self.HX7.set_reference_unit_A(reference_unit=float(4819+4/15))
		# Second Load Cell Calibration
		# self.HX7.set_reference_unit_A(reference_unit=float(3298.38333333))
		# self.HX7.tare()

	def init_GPIO(self):
		#initialize the pins used for the Hbridge
		self.L_EN = led(self.GPIO4)
		self.LPWM = out(self.GPIO17, frequency = self.frequency)
		self.R_EN = led(self.GPIO27)
		self.RPWM = out(self.GPIO22, frequency = self.frequency)
		self.L_EN.on()
		self.R_EN.on()
		self.LED = led(self.GPIO24)
		self.LED.on()
		#initiatilize the buttons
		self.start= gp(self.GPIO25, bounce_time = self.bounce)
		self.start.when_pressed = self.FORWARD

		self.stop = gp(self.GPIO13, bounce_time = self.bounce)
		self.stop.when_pressed = self.BRAKE
		self.reverse = gp(self.GPIO12, bounce_time = self.bounce)
		self.reverse.when_pressed = self.BACKWARD
		# For trying the motor class
		# self.motor = Motor(17, 22)
	
	def updateWeight(self):
		self.weight= self.HX7.get_weight(3)
		if self.weight2 < self.weight:
			self.weight2 = self.weight
		self.lbl_weight.display(self.weight2)

	# This is the Refresh button that gets the current data
	def GET_DATA(self):

		# Connect to Sqlite3 database and fill GUI table with data.
		db=sqlite3.connect("psbcscores.db")
		cursor=db.cursor()

		command=''' SELECT * from scores ORDER BY weight DESC'''

		result=cursor.execute(command)

		self.table.setRowCount(0)

		for row_number, row_data in enumerate(result):
			self.table.insertRow(row_number)
			for column_number, data in enumerate(row_data):
				(self.table.setItem(row_number, column_number,
				QTableWidgetItem(str(data))))

	def START(self):
		self.weight2 = 0
		self.RPWM.value = self.off
		self.LPWM.value = self.PWM
		self.timer.start(50)
		self.HX7.tare()
		self.start_btn.setEnabled(False)

	def STOP(self):
		self.timer.stop()
		self.RPWM.value = self.LPWM.value = self.off
		self.start_btn.setEnabled(True)

	def REVERSE(self):
		# pass
		self.RPWM.value= self.PWM
		self.LPWM.value = self.off

	def ADD(self):
		# This adds the textbox info into the database
		# Currently only the TeamName and Weight
		db=sqlite3.connect("psbcscores.db")
		cursor=db.cursor()

		team_name_=self.teamname_txt.text()
		weight_=self.weight_txt.text()
		# load_=self.load.value()
		# category_=self.category.value()
		# teamnumber_=self.teamnumber.value()

		row=(team_name_,weight_)

		command=''' INSERT INTO scores (teamname,weight) VALUES (?,?) '''

		cursor.execute(command,row)

		db.commit()
		# pass

	# This is for the Test tab.
	def BACKWARD(self):
		self.RPWM.value= self.PWM
		self.LPWM.value = self.off
		# print(self.LPWM.is_active)
		# print(self.RPWM.is_active)
		# pass

	def FORWARD(self):
		# pass
		self.RPWM.value = self.off
		self.LPWM.value = self.PWM
		print(self.LPWM.is_active)
		print(self.RPWM.is_active)
		# motor.forward()

	def BRAKE(self):
		# pass
		self.RPWM.value = self.LPWM.value = self.off
		print(self.LPWM.is_active)
		print(self.RPWM.is_active)
		# motor.stop()

	def CALIBRATE(self):
		# we are going to use a known weight and a magic number to calibrate
        # # the load cell. no way around this.
		# weight = self.HX7.get_weight_A(5)
        # # in this line put in the reference weight
        # # leave the rest alone, the magic happens there
		# self.reference = weight * float(2.2046)
		# print(self.reference)
		# self.CValue = self.reference
		#we are going to use a known weight and a magic number to calibrate the load cell. no way around this.
		# self.HX7.tare()
		weight = self.HX7.get_weight(5)
		# HX7.set_reference_unit(self.reference)#this line is how you set the referencce number you divide by this
		self.lbl_weight.display(weight)
		# print(weight)
		# print(self.HX7.REFERENCE_UNIT)


    # this is the automation in the program
    # From Here on, this is the stuff that concerns the buttons.

	def StartRead(self):
		self.start = None  # Turn off the start button.
		ReadOp()  # Not sure what this is, will investigate -Jim

	def TurnoffAll(self):
	    self.destroy()

	# From here on this is the stuff that concerns the HX711/Load Cell
	def GetValue(self):
	    self.CValue = self.HX7.getValue()  # the Value straight from the ADC

	def TARE(self):
		self.HX7.tare(10)
		weight = self.HX7.get_weight(10)
		self.lbl_weight.display(weight)
		# print(self.HX7.REFERENCE_UNIT)


def main():

	app=QApplication(sys.argv)
	window=Main()
	window.show()
	app.exec_()

if __name__=='__main__':
	main()
